/**
 * Created by MniLL on 06.12.14.
 */

var nStatic = require('node-static');
var http = require('http');
var file = new nStatic.Server('./public');
var config = require('./config');
var path = require('path');
var fs = require('fs');
var filePath = path.join(__dirname, "/peer-rtc-id-counter-pid.json");


var peerIdCounters = {id: 0}; //Special object, for unique id, even after server restarted; Saved in file at filepath
fs.readFile(filePath, 'utf8', function (err, data) {
    if (!err) {
        try {
            peerIdCounters = JSON.parse(data);
        } catch (e){
            //error parse json, just create new.
        }
    }
    if (!peerIdCounters)
        peerIdCounters = {id: 0};
});

var app = http.createServer(function (req, res) {
    file.serve(req, res);
}).listen(config.get("port"));

var clientsPool = [];
var io = require('socket.io').listen(app);

io.sockets.on('connection', function (socket) {

    //socket.on('message', function (message) {
    //    socket.broadcast.emit('message', message);
    //});

    socket.on('join', function (selectedClass) {
        if (isCorrectedClass(selectedClass) && !socket.pooled){
            clientsPool.push({id:socket.id, playerClass:selectedClass, peerId:socket.peerId});
            socket.pooled = true;
            if (clientsPool.length >=2)
                createGame();
        } else {
            console.log('Wrong class or double connection');
        }
    });

    socket.on('disconnect', function() {
        if (socket.pooled){
            for (var key in clientsPool)
                if (clientsPool.hasOwnProperty(key))
                    if (clientsPool[key].id == socket.id){
                        clientsPool.splice(clientsPool.indexOf(key), 1);
                        socket.pooled = false;
                        break;
                    }
        }
    });

    socket.peerId = getId();
    socket.emit('peerId', socket.peerId);
});

function isCorrectedClass(cClass){
    return cClass === 'tank' || cClass ==='medic' || cClass === 'marine';
}

function createGame(){
    var server, client1, client2, client3;
    if (clientsPool.length < 2)
        return false;

    server = clientsPool.pop();
    client1 = clientsPool.pop();
    io.to(server.id).emit('startGame', JSON.stringify({isServer:true, playerClass:server.playerClass, peerId:server.peerId, clients:[{id:client1.peerId, playerClass:client1.playerClass}]}));
    io.to(client1.id).emit('startGame', JSON.stringify({isServer:false, playerClass:server.playerClass, peerId:client1.peerId, server:server.peerId}));
}

//TODO Make with redis or mamecache or ...
function getId(){
    peerIdCounters.id ++;
    fs.writeFile(filePath, JSON.stringify(peerIdCounters, null, 4), function(err) {
        if(err) {
            console.log(err);
        }
    });
    return peerIdCounters.id;
}