/**
 * Created by MniLL on 06.12.14.
 */
var Game; //Init global game var
(function() {
    Game = {
        sprites: [],
        animations: [],
        atlases: [],
        enemyList: [],
        friendlyList: [],
        touchList: [], // list of touches
        guiList: [], 	//
        environmentList: [],
        cycleTickTime: 50, //cycle time(setTimeout)
        noTouch: 0,  //times without touches in ms
        lastDrawTime: Date.now(),
        redrawTime:50,
        random: 10, //Counter for determinated random
        randomCounter: 10,
        width: 1024, //window width
        height: 768, //game height
        windowWidth: window.innerWidth,
        windowHeight: window.innerHeight,
        heightShift: 1536/(window.innerWidth/(window.innerHeight*4/3)),
        scale: (window.innerWidth)/1024,
        mapWidth: 1024, //Tile counts in row
        mapHeight: 768, //Tile counts in column
        gameOptions: '',
        dataConnections: [],
        serverConnection: '',
        peer: '',
        gameState: 'init',
        lastDate: Date.now(),
        debug: true,
        guiGroups: []
    }
})();

