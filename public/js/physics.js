//Copy pasted from my another project :'-( My standard pVec2 library.

function Vec2(x, y) {
    this.x = x;
    this.y = y;
}

// Get rotate from move vector!
Vec2.prototype.getRotate = function (){
    var rotate = Math.acos((0 + this.y)/(Math.sqrt((Math.pow(0,2)+Math.pow(1,2))*(Math.pow(this.x,2)+Math.pow(this.y,2)))));
    if (this.x < 0)
        rotate = Math.PI*2-rotate;
    if (isNaN(rotate))
        return 0;
    return Math.PI -rotate;
};


Vec2.prototype.getRotated = function (angle){
    return this.clone().rotate(-angle);
};


Vec2.prototype.rotate = function (angle){
    var cachePoint = new Vec2(this.x, this.y);
    this.x  = cachePoint.x * Math.cos(angle) - cachePoint.y * Math.sin(angle);
    this.y  = cachePoint.x * Math.sin(angle) + cachePoint.y * Math.cos(angle);
    return this;
};


Vec2.prototype.multiply = function (factor){
    this.x*=factor;
    this.y*=factor;
    return this;
};


Vec2.prototype.multiplyed = function (factor){
    return new Vec2(this.x*factor, this.y*factor);
};


Vec2.prototype.add = function (vecToAdd){
    if (!(vecToAdd instanceof Vec2)&&(arguments.length == 2))
        vecToAdd = new Vec2(arguments[0], arguments[1]);
    this.x+=vecToAdd.x;
    this.y+=vecToAdd.y;
    return this;
};


Vec2.prototype.sub = function (vecToSub){
    this.x-=vecToSub.x;
    this.y-=vecToSub.y;
    return this;
};


Vec2.prototype.clone = function () {
    return new Vec2(this.x, this.y);
};

Vec2.prototype.set = function (vec) {
    this.x = vec.x;
    this.y = vec.y;
    return this;
};


Vec2.prototype.normalize = function (){
    var length = this.getLength();
    if (length){
        this.multiply(1/this.getLength());
    }
    return this;
};


Vec2.prototype.getLength = function (){
    return Math.sqrt(Math.pow(this.x,2)+Math.pow(this.y,2));
};