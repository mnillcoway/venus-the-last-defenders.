/**
 * Created by MniLL on 07.12.14.
 */

var Background = function () {
    this.ctx = new PIXI.RenderTexture(1024, 768);
    this.init();
    this.sprite = new PIXI.Sprite(this.ctx);
};

Background.prototype.init = function (){
    var spr = PIXI.Sprite.fromFrame('bg');
    this.ctx.render(spr);
};

Background.prototype.drawOn = function (sprite, position, rotation) {
    var spr = Game.pool.getSprite(sprite);
    var doc = new PIXI.DisplayObjectContainer();
    spr.position = position;
    spr.rotation = rotation || 0;
    doc.addChild(spr);
    this.ctx.render(doc);
};
