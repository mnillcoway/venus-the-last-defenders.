/**
 * Created by MniLL on 06.12.14.
 */
(function(){
    Game.socket = io.connect(window.location.origin, {port: 8080});

    function initPeer(id){
        var peer = new Peer(id, {host: window.location.hostname, port:9000});
        peer.on('connection', function(dataConnection) {
            if (!Game.gameOptions.isServer && parseInt(dataConnection.peer) == Game.gameOptions.server) {
                initDataConnection(dataConnection, true);
            } else {
                dataConnection.close();
            }
        });
        peer.on('call', function(mediaConnection){
            mediaConnection.close();
        });
        peer.on('close', function(){
            console.log('peer close');
        });
        peer.on('disconnected', function(){
            console.log('signal disconnected');
        });

        peer.on('error', function(err){
            if (err.type == 'browser-incompatible'){
                alert('browser-incompatible');
            }
            console.error('peer error');
            console.log(err);
        });
        return peer;
    }

    function initDataConnection(connection, fromServer){
        connection.on('data', function(data) {
            Game.globallogic.onData(data, this);
        });
        connection.on('close', function() {
            Game.globallogic.connectionClose(this);
        });
        connection.on('error', function(err) {
            console.log(data);
        });
        if (fromServer) {
            Game.globallogic.state = 'connection';
            Game.serverConnection = connection;
        } else {
            Game.globallogic.playerConnection(connection);
        }
    }

    Game.socket.on('startGame', function (message) {
            try {
                Game.gameOptions = JSON.parse(message);
            } catch (e) {
                alert('Wrong game options received');
            }
            Game.globallogic.startGame(Game.gameOptions);
            if (Game.gameOptions.isServer) {
                setTimeout(function() { //TODO Make ready, without timeout
                    Game.gameOptions.clients.forEach(function (client) {
                        initDataConnection(Game.peer.connect(client.id, {reliable: true}), false);
                    });
                }, 5000);
            }
            Game.socket.disconnect();
    });

    Game.socket.on('peerId', function(message){
        Game.peer = initPeer(parseInt(message));
    })
})();