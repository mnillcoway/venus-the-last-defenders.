/**
 * Created by MniLL on 07.12.14.
 */

function Bullet (bullet, startPost, endPost, target, damage, end){
    this.position = startPost;
    this.endPosition = endPost;
    this.onEnd = end;
    this.target = target;
    this.speed = bullet.speed;
    this.damage = damage;
    if (bullet.type == 'sprite')
        this.displayObject = Game.pool.getSprite(bullet.name);
    else
        this.displayObject = Game.pool.getAnimation(bullet.name);

    Game.effectContainer.addChild(this.displayObject);
    this.displayObject.position = this.position;
    Game.lvl.objEffect.push(this);
    var direction = this.endPosition.clone().sub(this.position);
    this.displayObject.rotation = direction.getRotate();
}

Bullet.prototype.update = function (){
    var direction = this.endPosition.clone().sub(this.position);
    var targetRange = this.target.position.clone().sub(this.position);
    this.displayObject.rotation = direction.getRotate();
    if (direction.getLength() > this.speed && targetRange.getLength() > this.speed) {
        this.position.add(direction.normalize().multiply(this.speed));
    } else {
        Utils.deleteEl(Game.lvl.objEffect, this);
        Game.pool.pull(this.displayObject);

        if (this.onEnd)
            this.onEnd();

        if (this.target)
            this.target.applyDamage(this.damage || 0)
    }
};