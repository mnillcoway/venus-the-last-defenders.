/**
 * Created by MniLL on 06.12.14.
 */

GlobalLogic =  function() {
    this.isServer = false;
    this.state = 'init'; //States init, search, connection, loading, game, waiting, lose.
    this.players = [];
    this.lastRound = -1;
    this.init();
    this.actions = [];
    this.roundData = [[]]
};

GlobalLogic.prototype.init = function(){
    this.initWelcomeScreen();
};

GlobalLogic.prototype.update = function(){
    var i = 0;
    switch (this.state){
        case 'init':
            break;
        case 'search': case 'connection': case 'loading':
        if (this.pendingTimer){
            this.pendingTimer++;
            var dots = '.', dotsCounter = parseInt((this.pendingTimer % 60) / 20) ;
            if (dotsCounter > 0)
            {
                dots += dotsCounter == 1 ? '.' : '..';
            }
            this.initLoadingScreen(this.state+dots);
        } else {
            this.pendingTimer = 1;
        }
        if (Game.gameOptions.isServer && this.state == 'connection'){
            for (i = this.players.length -1; i>=0; i--){
                if (!this.players[i].connect)
                    this.players[i].waiting++;

                if (this.players[i].waiting > 600){
                    this.players[i].connection.close();
                    Utils.deleteEl(this.players, this.players[i]);
                }
            }
        }
        if (Game.gameOptions.isServer && this.state == 'connection'){
            var waitingFor =0;
            for (i = this.players.length -1; i>=0; i--){
                if (!this.players[i].connect) {
                    this.players[i].waiting++;
                    waitingFor++;
                }
                if (this.players[i].waiting > 600){
                    this.players[i].connection.close();
                    Utils.deleteEl(this.players, this.players[i]);
                }
            }
            if (waitingFor == 0){
                Game.globallogic.initLvl();
            }
        }
        break;
        case 'game':
            if (Game.lvl) {
                if (parseInt(Game.lvl.round / 5) != this.lastRound){
                    if (this.roundData[parseInt(Game.lvl.round / 5)]) {
                        this.makeActions(this.roundData[parseInt(Game.lvl.round / 5)]);


                        //Spawn enemy
                        if (Math.random()*20 > 17) {
                            var enemyName = (Math.random() * 10) < 8 ? ((Math.random() * 10) < 5 ? 'clone' : 'droid') : 'mutant';
                            this.actions.push({name: 'spawn', args: [enemyName, Math.random() * 900 + 80, -40]});
                        }

                        this.lastRound = parseInt(Game.lvl.round / 5);
                        if (!this.isServer) {
                            Game.serverConnection.send({
                                message: 'round',
                                number: this.lastRound+1,
                                player: {id: Game.gameOptions.peerId, actions: this.actions}
                            });
                            this.actions = [];
                        }
                    }
                }

                if (this.roundData[parseInt(Game.lvl.round / 5)]) {
                    if (this.waitingText) {
                        Game.stage.removeChild(this.waitingText);
                        this.waitingText = null;
                    }
                    Game.lvl.update();
                } else {
                    if (!this.waitingText) {
                        this.waitingText = new PIXI.Text('Waiting',{
                            font: Utils.getFont(64),
                            align: 'left',
                            fill: 'Yellow',
                            width: 400
                        });
                        Game.stage.addChild(this.waitingText);
                        this.waitingText.position.y = 300;
                        this.waitingText.position.x = 600;
                    }

                }

                if (this.isServer)
                    this.checkNextRoundData();
            }
            break;
        case 'waiting':
            break;
        case 'lose':
            this.initLoadingScreen(this.state);
            break;
        case 'disconnected':
            this.initLoadingScreen(this.state);
            break;

    }

};

GlobalLogic.prototype.makeActions = function(actions){
    actions.forEach(function(actionsData){
        var player = Game.lvl.getPlayerWithId(actionsData.id);
        actionsData.actions.forEach(function(action){
            if (Game.globallogic.isValidAction(action.name)){
                if (action.name == 'spawn'){
                    Game.lvl.objEnemy.push(new Enemy(Game.enemyList[action.args[0]], new Vec2(action.args[1], action.args[2])));
                } else if (action.name == 'move'){
                    player.destinationPoint = new Vec2(action.args[0], action.args[1]);
                }
            }
        });
    });
};

GlobalLogic.prototype.isValidAction = function(name){
    //TODO validate;
    return true;
};

GlobalLogic.prototype.checkNextRoundData = function (){
    if (this.roundData[this.lastRound+1])
        return;

    var waitingFor = 0;

    for (var i=0; i< this.players.length; i++){
        if (this.players[i].connect && !this.players[i].roundData[this.lastRound+1]) {
            waitingFor++;
            if (parseInt(Game.lvl.round / 5) != this.lastRound) {
                this.players[i].waiting ++;
                if (this.players[i].waiting > 200) {
                    this.players[i].connect = false;
                    this.players[i].connection.close();
                }
            }
        }
    }

    if (waitingFor == 0) {
        var data = this.generateRoundData(this.lastRound+1);
        this.roundData[this.lastRound+1] = data.players;
        this.brodcast(data);
    }
};

GlobalLogic.prototype.generateRoundData = function (){
    var data = {message: 'round', number:this.lastRound+1, players:[]};
    var server = {id:Game.gameOptions.peerId, actions:this.actions};
    this.actions = [];
    data.players.push(server);
    for (var i=0; i< this.players.length; i++){
        if (this.players[i].connect && this.players[i].roundData[this.lastRound+1]) {
            data.players.push(this.players[i].roundData[this.lastRound+1]);
        }
    }
    return data;
};

GlobalLogic.prototype.selectClass = function (name){
    this.selectedCharacter = name;
    if (this.selectedCharacterName)
        this.selectedCharacterName = Game.pool.switchText(new PIXI.Text(name || '', {
            font: Utils.getFont(64),
            align: 'center',
            fill: 'Red'
        }), this.selectedCharacterName);

    this.starButton.setState(name ? 'idle' : 'disabled');
};

GlobalLogic.prototype.searchGame = function(){
    if (!this.selectedCharacter)
        return false;

    this.state = 'search';
    this.initLoadingScreen(this.state);
    Game.socket.emit('join', this.selectedCharacter); //DEBUG
};

GlobalLogic.prototype.startGame = function (data) {
    this.isServer = data.isServer;
};

GlobalLogic.prototype.playerConnection = function(connection) {
    var clientCharacter;
    Game.gameOptions.clients.forEach(function(client){
        if (client.id == parseInt(connection.peer))
            clientCharacter = client;
    });
    if (clientCharacter)
        this.players.push(new Player(connection, clientCharacter.playerClass));

    connection.on('open', function() {
        Game.globallogic.playerReady(this);
    });

    if (this.players.length == Game.gameOptions.clients.length){
        this.state = 'connection';
    }
};

GlobalLogic.prototype.playerReady = function (connection) {
    for (var key in this.players){
        if (this.players.hasOwnProperty(key) && this.players[key].connection == connection){
            this.players[key].connect = true;
        }
    }
};

GlobalLogic.prototype.connectionClose = function (connection) {
    if (this.isServer) {
        for (var key in this.players) {
            if (this.players.hasOwnProperty(key) && this.players[key].connection == connection) {
                this.players[key].connect = false;
            }
        }
    } else {
        Game.globallogic.serverIsGone();
    }
};

GlobalLogic.prototype.serverIsGone = function(){
    this.state = 'disconnected';
};

GlobalLogic.prototype.initLvl = function () {
    var gameMetadata = {message: 'start', players:[]};
    gameMetadata.players.push({id:Game.gameOptions.peerId, character:Game.gameOptions.playerClass, number:0});
    for (var i=0; i< this.players.length; i++){
        gameMetadata.players.push({id:this.players[i].id, character:this.players[i].character, number:i+1});
    }
    Game.globallogic.brodcast(gameMetadata);
    if (this.players.length > 0){
        this.state = 'loading';
    } else {
        this.state = 'game';
    }
    this.loadLvl(gameMetadata.players);
    Game.peer.disconnect(); // Disconect from signal server :'-)
};

GlobalLogic.prototype.brodcast = function (data) {
    this.players.forEach(function(player){
        if (player.connect)
            player.connection.send(data);
    })
};

GlobalLogic.prototype.onData = function (data, connection) {
    if (this.isServer){
        if (data.message && data.message == 'round') {
            for (var i = 0; i < this.players.length; i++) {
                if (this.players[i].id == parseInt(connection.peer)){
                    this.players[i].roundData[data.number] = data.player;
                }
            }
        }
    } else {
        if (data.message &&(data.message == 'start')&&(this.state = 'connection')){
            this.state = 'loading';
            this.loadLvl(data.players)
        } else if (data.message && data.message == 'round'){
            this.roundData[data.number] = data.players;
        }
    }
};

GlobalLogic.prototype.loadLvl = function (data) {
    Game.globallogic.state = 'game';
    Game.lvl = new Lvl(data, function() {
        Game.globallogic.character = Game.lvl.getPlayerWithId(Game.gameContainer.peerId);
    });
};

GlobalLogic.prototype.initWelcomeScreen = function () {
    var fon;

    for (var i = Game.stage.children.length-1; i>=0; i--)
        Game.stage.removeChild(Game.stage.children[i]);

    var bkg = Game.pool.getSprite('bg_menu');
    bkg.position.x = Game.windowWidth/2;
    bkg.position.y = Game.windowHeight/2;
    Game.stage.addChild(bkg);
    var medicIcon = new Gui(Game.guiList['medicIcon']);
    Game.stage.addChild(medicIcon.display);
    var marineIcon = new Gui(Game.guiList['marineIcon']);
    Game.stage.addChild(marineIcon.display);
    var tankIcon = new Gui(Game.guiList['tankIcon']);
    Game.stage.addChild(tankIcon.display);
    Game.guiGroups['selectClass'] = [];
    Game.guiGroups['selectClass'].push(medicIcon);
    Game.guiGroups['selectClass'].push(marineIcon);
    Game.guiGroups['selectClass'].push(tankIcon);
    var selectText = new PIXI.Text('Select character class', {
        font: Utils.getFont(64),
        align: 'center',
        fill: 'Yellow'
    });
    selectText.position = new Vec2(542, 50);
    selectText.anchor.x = 0.5;
    Game.stage.addChild(selectText);
    this.selectedCharacterName = new PIXI.Text('', {
        font: Utils.getFont(64),
        align: 'center',
        fill: 'Red'
    });
    this.selectedCharacterName.position = new Vec2(542, 350);
    Game.stage.addChild(this.selectedCharacterName);

    var startButton = new Gui(Game.guiList.startGame);
    this.starButton = startButton;
    Game.stage.addChild(startButton.display);
};

GlobalLogic.prototype.initLoadingScreen = function (Text) {
    var fon;
    for (var i = Game.stage.children.length-1; i>=0; i--)
        Game.stage.removeChild(Game.stage.children[i]);
    var bkg = Game.pool.getSprite('bg_menu');
    bkg.position.x = Game.windowWidth/2;
    bkg.position.y = Game.windowHeight/2;
    Game.stage.addChild(bkg);
    var selectText = new PIXI.Text(Text, {
        font: Utils.getFont(64),
        align: 'left',
        fill: 'Yellow',
        width: 400
    });
    selectText.position = new Vec2(550, 700);
    Game.stage.addChild(selectText);
};


Player = function (connection, character){
    this.connection = connection;
    this.character = character;
    this.id = connection.peer;
    this.connect = false;
    this.loaded = false;
    this.waiting = 0;
    this.roundData = [];
};