/**
 * Created by MniLL on 07.12.14.
 */

Game.loadAssets = function (callback) {
    var key;
    Game.backgrounds = {};

    var loadAtlases = function(data){

        for(key in data.sprites){
            Game.sprites[data.sprites[key].name] = data.sprites[key];
        }

        for(key in data.atlases){
            Game.atlases.push(new Atlas(data.atlases[key].file, data.atlases[key].tag))
        }

        for(key in data.animations){
            Game.animations[data.animations[key].name] = new Animation(data.animations[key]);
            Game.animations[data.animations[key].name].name = data.animations[key].name;
        }

        if (Game.debug)
            console.log('Atlases info loaded');
        pixiLoader();
    }.bind(this);


    var pixiLoader = function(){
        var spriteCount = 0, atlas;
        var onLoad = function () {

            spriteCount--;

            if (spriteCount == 0) {
                callback();
            }
        };

        for (atlas in Game.atlases)
                spriteCount++;

        for (atlas in Game.atlases)
                Game.atlases[atlas].load(onLoad);
    };

    var loadJSON = function (path, success, error)
    {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function()
        {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    if (success)
                        success(JSON.parse(xhr.responseText));
                } else {
                    if (error)
                        error(xhr);
                }
            }
        };
        xhr.open("GET", path, true);
        xhr.send();
    };
    loadJSON("atlases/info.json", loadAtlases, null);


    function Atlas(fileName, tag) {
        this.fileName = fileName;
        this.tag = tag;
        this.loaded = false;
    }

    Atlas.prototype.load = function(onLoad){
        var loader = new PIXI.JsonLoader(this.fileName);
        loader.on('loaded', onLoad);
        loader.load();
        this.loaded = true;
    };

    function Animation(data) {
        this.sprites = data.sprites;
        this.playTime = data.playTime;
        this.count = data.sprites.length;
    }

};