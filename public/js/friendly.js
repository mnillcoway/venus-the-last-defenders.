/**
 * Created by MniLL on 07.12.14.
 */

function Character (params, data) {
    this.params = params;
    this.animations = params.animations;
    this.animation = Game.pool.getAnimation(this.animations['idle']);
    Game.gameContainer.addChild(this.animation);
    this.position = new Vec2(150+data.number*150, 600);
    this.animation.position = this.position;
    this.hitPoints = params.hitPoints;
    this.maxHitPoints = params.hitPoints;
    this.damage = params.damage;
    this.attackTimeOut = params.attackTimeOut;
    this.destinationPoint = null;
    this.speed = params.speed;
    this.target = null;
    this.init(params);
}

Character.prototype.init  = function (params) {
    if (params.init)
        params.init.call(this);
};

Character.prototype.move = function() {
    if (this.destinationPoint){
        this.animation = Game.pool.switchAnimation(this.animations['run'], this.animation);
        var direction = this.destinationPoint.clone().sub(this.position);
        this.animation.rotation = direction.getRotate();
        if (direction.getLength() > this.speed){
            this.position.add(direction.normalize().multiply(this.speed));
        } else {
            this.position.set(this.destinationPoint);
            this.destinationPoint = null;
        }
    }
};

Character.prototype.update = function () {
    if (this.hitPoints > 0)
        this.strategy();
};

Character.prototype.applyDamage = function (damage){
    this.hitPoints -= damage;

    if (this.hitPoints<=0) {
        if(!this.alreadyDead)
            this.onDeath();
        this.alreadyDead = true;
    }
};

Character.prototype.onDeath = function (){
    this.animation = Game.pool.switchAnimation(this.animations['die'], this.animation);
    this.animation.loop = false;
};

Character.prototype.scanEnemyAround = function () {
    if(this.target && this.target.hitPoints > 0)
        return;

    this.target = null;
    var lastLength;
    Game.lvl.objEnemy.forEach(function (enemy) {
        if (!lastLength || enemy.position.clone().sub(this.position).getLength() < lastLength){
            this.target = enemy;
            lastLength = enemy.position.clone().sub(this.position).getLength();
        }
    }.bind(this));
};

Character.prototype.strategy = function (){
    this.timeOut -= Game.cycleTickTime;

    if (this.timeOut > 0)
        return;

    this.scanEnemyAround();

    if (this.destinationPoint){
      this.move();
    } else if (this.target) {
        this.animation.rotation = this.target.position.clone().sub(this.position).getRotate();
        this.animation = Game.pool.switchAnimation(this.animations['shoot'], this.animation);
        this.timeOut = this.attackTimeOut;
        new Bullet(this.params.bullet, this.position.clone().add(this.target.position.clone().sub(this.position).normalize().multiply(100)), this.target.position.clone(), this.target, this.damage);
    } else {
        this.animation = Game.pool.switchAnimation(this.animations['idle'], this.animation);
    }
};