/**
 * Created by MniLL on 07.12.14.
 */


/**
 * Created by MniLL on 07.12.14.
 */

Game.enemyList.clone = {
    animations: {shoot:'cloneShoot', run:'cloneRun', die:'cloneDead'},
    bullet: {type:'sprite', name:'cloneAmmo', speed:20},
    attackRange: 300,
    damage: 2,
    attackTimeOut: 500,
    speed: 4,
    hitPoints: 20
};

Game.enemyList.droid = {
    animations: {shoot:'droidShoot', run:'droidRun', die:'droidDead'},
    bullet: {type:'sprite', name:'droidAmmo', speed:25},
    attackRange: 500,
    damage: 2,
    attackTimeOut: 500,
    speed: 3,
    hitPoints: 20
};

Game.enemyList.mutant = {
    animations: {shoot:'mutantShoot', run:'mutantRun', die:'mutantDead'},
    bullet: {type:'sprite', name:'mutantAmmo', speed:15},
    attackRange: 300,
    damage: 40,
    attackTimeOut: 1000,
    speed: 2,
    hitPoints: 300
};
