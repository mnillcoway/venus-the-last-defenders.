/**
 * Created by MniLL on 07.12.14.
 */

//Copy pasted from my old project :'-(

SpritePool = function(){
    this.animations = {};
    this.sprites = {};
};

SpritePool.prototype.pull = function(obj){
    if (obj instanceof PIXI.MovieClip){
        this.pullAnimation(obj);
    } else if (obj instanceof PIXI.Text){
        if (obj.parent)
            obj.parent.removeChild(obj);
    } else if (obj instanceof PIXI.Sprite){
        this.pullSprite(obj);
    } else {
        if (obj && obj.parent)
            obj.parent.removeChild(obj);
    }
};

SpritePool.prototype.getSprite = function(name){
    var sprite;
    if (!this.sprites[name])
        this.sprites[name] = [];

    if (this.sprites[name].length > 0) {
        sprite = this.sprites[name].pop();
    } else {
        sprite = PIXI.Sprite.fromFrame(name);
        sprite.anchor = new PIXI.Point(0.5, 0.5);
        sprite.pivot = sprite.texture.framePivot;
    }
    sprite.rotation = 0;
    sprite.name = name;
    return sprite;
};

SpritePool.prototype.pullSprite = function(sprite){
    if (!this.sprites[sprite.name])
        this.sprites[sprite.name] = [];

    this.sprites[sprite.name].push(sprite);
    if (sprite.parent)
        sprite.parent.removeChild(sprite);
};

SpritePool.prototype.switchSprite = function(name, sprite){
    if (sprite.name == name)
        return sprite;
    var newSprite = this.getSprite(name);
    newSprite.position = sprite.position;
    newSprite.rotation = sprite.rotation;
    if (sprite.parent)
        sprite.parent.addChildAt(newSprite, sprite.parent.children.indexOf(sprite));
    this.pullSprite(sprite);
    return newSprite;
};


SpritePool.prototype.getAnimation = function(name){
    var anim;

    if (!this.animations[name])
        this.animations[name] = [];

    if (this.animations[name].length > 0) {
        anim = this.animations[name].pop();
        anim.currentFrame = 0;
    } else {
        anim = PIXI.MovieClip.fromFrames(Game.animations[name].sprites);
        anim.animationSpeed = Game.animations[name].playTime;
        anim.name = name;
        anim.pivot = anim.texture.framePivot;
    }
    anim.play();
    return anim;
};

SpritePool.prototype.pullAnimation = function(animation){
    if (!this.animations[animation.name])
        this.animations[animation.name] = [];

    animation.stop();
    animation.onComplete = null;
    this.animations[animation.name].push(animation);
    if (animation.parent)
        animation.parent.removeChild(animation);
};

SpritePool.prototype.switchAnimation = function(name, anim){
    if (anim.name == name)
        return anim;

    var newAnim = this.getAnimation(name);
    newAnim.position = anim.position;
    newAnim.onComplete = anim.onComplete;
    newAnim.currentFrame = 0;
    newAnim.rotation = anim.rotation;
    if (anim.parent)
        anim.parent.addChildAt(newAnim, anim.parent.children.indexOf(anim));

    this.pullAnimation(anim);
    return newAnim;
};

SpritePool.prototype.switchText = function(newt, oldt){
    newt.position = oldt.position;
    newt.rotation = oldt.rotation;
    if (oldt.parent)
        oldt.parent.addChildAt(newt, oldt.parent.children.indexOf(oldt));

    if (oldt.parent)
        oldt.parent.removeChild(oldt);

    return newt;
};
