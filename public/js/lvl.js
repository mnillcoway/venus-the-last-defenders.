/**
 * Created by MniLL on 07.12.14.
 */


var Lvl = function(data, onLoad){
    var i;
    for (i = Game.stage.children.length-1; i>=0; i--)
        Game.stage.removeChild(Game.stage.children[i]);

    Game.cameraContainer = new PIXI.DisplayObjectContainer();
    Game.gameContainer = new PIXI.DisplayObjectContainer();
    Game.guiContainer = new PIXI.DisplayObjectContainer();
    Game.effectContainer = new PIXI.DisplayObjectContainer();
    Game.stage.addChild(Game.cameraContainer);
    Game.cameraContainer.addChild(Game.gameContainer);
    Game.cameraContainer.addChild(Game.effectContainer);
    Game.stage.addChild(Game.guiContainer);

    this.background = new Background();
    Game.gameContainer.addChild(this.background.sprite);

    Game.gameContainer.interactive = true;
    Game.gameContainer.click = Game.gameContainer.tap = function(event){
      Game.globallogic.actions.push({name:'move', args:[event.global.x, event.global.y]});
    };

    this.round = 0;
    this.objEnemy = [];
    this.characters = [];
    this.objFriendly = [];
    this.objCharges = [];
    this.objEffect = [];
    this.objGui = [];
    for (i = 0; i< data.length; i++) {
        var char = new Character(Game.friendlyList[data[i].character], data[i]);
        char.id = data[i].id;
        this.characters.push(char);
        this.objFriendly.push(char);
    }

    setTimeout(function(){
        onLoad();
    }, 1);
};

Lvl.prototype.getPlayerWithId = function (id){
    for (var i = 0; i< this.characters.length; i++){
        if (this.characters[i].id == id){
            return this.characters[i];
        }
    }
};

Lvl.prototype.update = function (id){
    this.round ++;
    var i;

    for (i = 0; i< this.objFriendly.length; i++)
        this.objFriendly[i].update();

    for (i = 0; i< this.objEnemy.length; i++)
        this.objEnemy[i].update();

    for (i = 0; i< this.objCharges.length; i++)
        this.objCharges[i].update();

    for (i = 0; i< this.objEffect.length; i++)
        this.objEffect[i].update();

    for (i = 0; i< this.objGui.length; i++)
        this.objGui[i].update();

};