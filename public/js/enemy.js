/**
 * Created by MniLL on 07.12.14.
 */

/**
 * Created by MniLL on 07.12.14.
 */

function Enemy (params, position) {
    this.params = params;
    this.animations = params.animations;
    this.animation = Game.pool.getAnimation(this.animations['run']);
    this.container = new PIXI.DisplayObjectContainer();
    this.container.addChild(this.animation);
    Game.gameContainer.addChild(this.container);
    this.position = position;
    this.animation.position = this.position;
    this.hitPoints = params.hitPoints;
    this.maxHitPoints = params.hitPoints;
    this.damage = params.damage;
    this.attackTimeOut = params.attackTimeOut;
    this.destinationPoint = null;
    this.speed = params.speed;
    this.attackRange = params.attackRange;
    this.target = null;
    //this.container.interactive = true;
    //this.container.click = this.container.tap = function(){
    //    console.log('click');
    //};
    this.init(params);
}

Enemy.prototype.init  = function (params) {
    if (params.init)
        params.init.call(this);
};

Enemy.prototype.move = function() {
    if (this.target) {
        this.animation = Game.pool.switchAnimation(this.animations['run'], this.animation);
        var direction = this.target.position.clone().sub(this.position);
        this.animation.rotation = direction.getRotate();
        this.position.add(direction.normalize().multiply(this.speed));
    }
};

Enemy.prototype.update = function (){
    this.strategy();
};

Enemy.prototype.applyDamage = function (damage){
    this.hitPoints -= damage;

    if (this.hitPoints<=0) {
        if(!this.alreadyDead)
            this.onDeath();
        this.alreadyDead = true;
    }
};

Enemy.prototype.onDeath = function (damage){
    Game.pool.pull(this.animation);
    Game.pool.pull(this.container);
    Utils.deleteEl(Game.lvl.objEnemy, this);
    Game.lvl.background.drawOn(this.animations.die, this.position, this.animation.rotation);
};


Enemy.prototype.scanEnemyAround = function () {
    if(this.target && this.target.hitPoints > 0)
        return;

    this.target = null;
    var lastLength;
    Game.lvl.objFriendly.forEach(function (enemy){
        if (enemy.hitPoints > 0)
            if (!lastLength || enemy.position.clone().sub(this.position).getLength() < lastLength){
               this.target = enemy;
               lastLength = enemy.position.clone().sub(this.position).getLength();
            }
    }.bind(this));
};

Enemy.prototype.strategy = function (){
    this.timeOut -= Game.cycleTickTime;

    if (this.timeOut > 0)
        return;

    this.scanEnemyAround();

    if (this.target && this.target.position.clone().sub(this.position).getLength() > this.attackRange){
        this.move();
    } else if (this.target) {
        this.animation.rotation = this.target.position.clone().sub(this.position).getRotate();
        this.animation = Game.pool.switchAnimation(this.animations['shoot'], this.animation);
        this.timeOut = this.attackTimeOut;
        new Bullet(this.params.bullet, this.position.clone().add(this.target.position.clone().sub(this.position).normalize().multiply(40)), this.target.position.clone(), this.target, this.damage);
    } else {
        this.animation = Game.pool.switchAnimation(this.animations['run'], this.animation);
    }
};


Enemy.prototype.onMap = function (){
    return this.x > 40 && this.x < Game.width - 40 && this.y > 40 && this.y < Game.height - 40;
};