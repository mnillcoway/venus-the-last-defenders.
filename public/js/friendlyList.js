/**
 * Created by MniLL on 07.12.14.
 */

Game.friendlyList.medic = {
    animations: {idle:'medicIdle', shoot:'medicShoot', run:'medicRun', die:'medicDie'},
    bullet: {type:'animation', name:'rocketFlame', speed:30},
    abilities: {
        1: function (){

        },
        2: function (){

        },
        3: function (){

        }
    },
    damage: 10,
    attackTimeOut: 500,
    speed: 7,
    hitPoints: 1000
};

Game.friendlyList.marine = {
    animations: {idle:'marineIdle', shoot:'marineShoot', run:'marineRun', die:'marineDie'},
    bullet: {type:'sprite', name:'turretAmmo', speed:30},
    abilities: {
        1: function (){

        },
        2: function (){

        },
        3: function (){

        }
    },
    damage: 10,
    attackTimeOut: 300,
    speed: 5,
    hitPoints: 1300
};

Game.friendlyList.tank = {
    animations: {idle:'tankIdle', shoot:'tankShoot', run:'tankRun', die:'tankDie'},
    bullet: {type:'sprite', name:'turretAmmo', speed:30},
    abilities: {
        1: function (){

        },
        2: function (){

        },
        3: function (){

        }
    },
    damage: 20,
    attackTimeOut: 1000,
    speed: 3,
    hitPoints: 2000
};