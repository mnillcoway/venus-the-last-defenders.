/**
 * Created by MniLL on 07.12.14.
 */

Game.guiList.selectClass = {
    init: function (params){
        this.state = 'idle';
        this.display = new PIXI.DisplayObjectContainer();
        this.className = params.className;
        this.position = new Vec2(params.x, params.y);
        this.display.position = this.position;
        this.currSprite = Game.pool.getSprite(params.icon);
        this.display.addChild(this.currSprite);
        this.display.buttonMode = true;
        this.display.click = this.display.tap = function (){
            if (this.state == 'idle') {
                Game.guiGroups['selectClass'].forEach(function(gui){
                   gui.unselect();
                });
                Game.globallogic.selectClass(this.className);
                this.currSprite = Game.pool.switchSprite(this.params.iconSelected, this.currSprite);
                this.state = 'selected';
            } else {
                Game.globallogic.selectClass(null);
                this.unselect();
            }
        }.bind(this);
        this.display.interactive = true;
        this.unselect = function (){
            if (this.state != 'idle ') {
                this.currSprite =  Game.pool.switchSprite(this.params.icon, this.currSprite);
                this.state ='idle';
            }
        }
    }
};

Game.guiList.medicIcon = {
    x: 330,
    y: 200,
    icon: 'medicIcon',
    iconSelected: 'medicIconSelected',
    className: 'medic',
    init: function(params){
        Game.guiList.selectClass.init.call(this, params);
    }
};

Game.guiList.marineIcon = {
    x: 530,
    y: 200,
    icon: 'tankIcon',
    iconSelected: 'tankIconSelected',
    className: 'tank',
    init: function(params){
        Game.guiList.selectClass.init.call(this, params);
    }
};

Game.guiList.tankIcon = {
    x: 730,
    y: 200,
    icon: 'marinesIcon',
    iconSelected: 'marinesIconSelected',
    className: 'marine',
    init: function(params){
        Game.guiList.selectClass.init.call(this, params);
    }
};

Game.guiList.startGame = {
    x: 540,
    y: 500,
    icon: 'startButton',
    init: function(params){
        this.state = 'idle';
        this.display = new PIXI.DisplayObjectContainer();
        this.position = new Vec2(params.x, params.y);
        this.display.position = this.position;
        this.currSprite = Game.pool.getSprite(params.icon);
        this.display.addChild(this.currSprite);
        this.display.buttonMode = true;
        this.text = new PIXI.Text('Start', {
            font: Utils.getFont(64),
            align: 'center',
            fill: 'Gray'
        });
        this.text.position.y = -24;
        this.display.addChild(this.text);
        this.display.click = this.display.tap = function (){
            if (this.state == 'idle') {
                Game.globallogic.searchGame();
            }
        }.bind(this);
        this.display.interactive = true;
        this.setState = function (state){
            this.state = state;
            var color = state != 'disabled' ? 'Gold' : 'Gray';
            this.text = Game.pool.switchText(new PIXI.Text('Start', {
                font: Utils.getFont(64),
                align: 'center',
                fill: color
            }), this.text);
        }
    }
};